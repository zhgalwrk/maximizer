<!DOCTYPE html>
<html lang="pl">
<!-- Maximizer -->

<head>
    <style class="vjs-styles-defaults">
        .video-js {
            width: 300px;
            height: 150px;
        }

        .vjs-fluid {
            padding-top: 56.25%
        }

        .subscribeBtn {
            font-weight: bold;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zostawiam Ci link do strony! Sprawdź to sam!</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="pl_site_files/reset.css">
    <link rel="stylesheet" href="pl_site_files/font-awesome.css">
    <link rel="stylesheet" href="pl_site_files/bootstrap.css">
    <link href="pl_site_files/css_003.css" rel="stylesheet" type="text/css">
    <link href="pl_site_files/css_002.css" rel="stylesheet" type="text/css">
    <link href="pl_site_files/css.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="pl_site_files/style.css">
    <link rel="stylesheet" href="pl_site_files/custom.css">
    <link rel="stylesheet" href="pl_site_files/custom1.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" crossorigin="anonymous">

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <script src="pl_site_files/videojs-ie8.js"></script>
    <script src="pl_site_files/video.js"></script>

    <link rel="stylesheet" href="f_css/intlTelInput.css">
    <link rel="stylesheet" href="f_css/style.css">

    <?=$neogara?>
    <?=$metrika?>
</head>

<body class="">

    <div class="modalWindow preloader">
        <div class="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div class="modalBg"></div>
    </div>



    <div class="count left copies" style="left: 201.5px; top: 80px;">
        <div class="value" id="copies">30</div>
        <div class="desc">
            Darmowe kopie są dostępne
        </div>
    </div>

    <div class="count right member_earn" style="width: 250px; right: 201.5px; top: 80px;">
        <!-- <img src="pl_site_files/people10.png" alt="" id="member_img"> -->

        <div class="desc">
            <div class="name"><span id="w_name">Samuel T.</span> Właśnie zarobił:</div>
            <div class="value">
                ‎€<span id="money">51.11</span>
            </div>
        </div>
    </div>
    <style>
        .header_content {
            display: flex;
            align-items: center;
        }
        .section_video{
            margin-top: 40px
        }
        @media screen and (max-width: 1200px) {

            .header_content {
                flex-direction: column;
                text-align: center
            }

            .header_slogan {
                margin: 0 auto;
            }
        }
    </style>
    <div id="wrapper">
        <div id="header">
            <div class="header_inner">
                <div class="header_content">
                    <div id="logo">
                        <!-- <a href="#"></a> -->
                        <img src="./images/logo.png" />
                    </div>

                    <div class="header_slogan">
                        <div class="trim_spaces">
                            <div class="scalable">
                                <div class="header_slogan_inner">
                                    <div style="font-size: 22px;">Rewolucyjny program doprowadzi Cię do<span>ponad 2500
                                            USD miesięcznie STABILNIE!</span> </div>
                                    <div style="font-size: 15px;text-transform:uppercase; text-align: center">Zarejestruj się i uzyskaj
                                        <span> bezpłatny dostęp!</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div id="container">

            <div class="section section_video">
                <div class="section_container">
                    <div class="video_wrapper">
                        <div class="video_title">
                            <div class="trim_spaces">
                                <div class="scalable">
                                    <div class="scalable">
                                        <span id="watchers">2124</span> w <b>Polsce</b> oglądają teraz to wideo
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="video">
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe src="videoloc.php" frameborder="0"
                                    style="width:100%;height:100%;position:absolute;border:none;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="section section_lp_reg_form">
                <div class="section_container">
                    <div class="section_title">
                        <span>Ostatnia szansa na dostanie się do zespołu</span>
                    </div>
                    <form class="f_form popup_form r_form neo_form" action="send.php" method="post">
                        <?=$hiddens?>
                        <div class="col-xs-12">
                            <input type="text" class="js-name" id="name" placeholder="Imię" name="firstname" required />
                        </div>
                        <div class="col-xs-12">
                            <input type="text" class="js-lastname" id="lastname" placeholder="Nazwisko" required
                                name="lastname" />
                        </div>
                        <div class="col-xs-12">
                            <input type="email" class="js-email" id="email" placeholder="Podaj swój e-mail" required
                                name="email" />
                        </div>
                        <div class="col-xs-12">
                            <input type="tel" class="js-phone" id="phone" name="phone_number" minlength="7" required />
                        </div>
                        <div class="col-xs-12 offer_row">
                            <input type="checkbox" name="oferta" checked="" class="js-oferta" />
                            <span style="color: #fff;">Wyrażam zgodę na przetwarzanie danych osobowych i otrzymywanie
                                materiałów reklamowych oraz zgadzam się z <a href="oferta.php" style="color: #fff;"
                                    class="f_lnk" target="_blank">ofertą publiczną</a></span>
                        </div>
                        <div class="col-xs-12">
                            <input type="submit" class="subscribeBtn" name="submitBtn"
                                value="Uzyskaj natychmiastowy dostęp">
                        </div>
                    </form>

                    <div class="images">
                        <img src="pl_site_files/payment.png" alt="">
                        <img src="pl_site_files/verified.png" alt="">
                        <img src="pl_site_files/safe.png" alt="">
                    </div>

                </div>
            </div>

        </div>
    </div>

    <script src="pl_site_files/jquery-1.js"></script>
    <script src="pl_site_files/bootstrap.js"></script>
    <script src="pl_site_files/scripts.js"></script>

    <script src="f_js/intlTelInput.min.js"></script>
    <script src="f_js/jquery.mask.min.js"></script>
    <script src="f_js/f_js.js"></script>
    <style>
        .formWrapper {
            max-width: 430px;
        }

        .f_form {
            max-width: 430px;
        }

        .f_form .offer_row {
            /*padding: 0;*/
            text-align: left;
            position: relative;
            top: -10px;
        }

        .f_form .offer_row input {
            display: inline-block;
            width: auto;
            float: left;
            margin: 0;
            margin-right: 5px;
        }

        input.js-oferta {
            width: auto !important;
            height: auto !important;
        }

        .f_form .offer_row span {
            font-size: 11px;
            line-height: 14px;
            font-family: sans-serif;
            color: #333;
            position: relative;
            top: -1px;
            display: block;
        }

        .f_form .offer_row a {
            color: #333;
        }
    </style>

    <script>
        $("input[type=tel]").intlTelInput({
            autoFormat: true,
            autoPlaceholder: "aggressive",
            defaultCountry: "auto",
            geoIpLookup: function (callback) {
                $.get('https://ipinfo.io', function () {}, "jsonp").always(function (
                    resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            nationalMode: false,
            hiddenInput: "phone",
            numberType: "MOBILE",
            utilsScript: "f_js/utils.js"
        });
    </script>
    <style>
        .country-list li.country {
            display: flex;
            align-items: center;
        }
    </style>
    <script>
        var ot = '3f344e6f5dfbbdea4bcf160c736ca617';
        var tt = 0;

        $(document).ready(function () {
            $('#videoblock').contextmenu(function () {
                return false;
            });

            $('a.outyes').click(function () {
                yesyoucan = 0;
            });
        });

        function rand(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }



        function copies() {

            var el = $("#copies");
            var left = parseInt($(el).html());

            left = left > 5 ? left - rand(1, 3) : left - rand(-2, 2);
            if (left < 2) {
                $(el).html(1);
            } else $(el).html(left);

            $("#watchers").html(rand(701, 1998));

            setTimeout('copies()', rand(6000, 9000));
        }

        var names = ["Melissa", "Michael", "Brian", "Tiffany", "Anthony", "Melissa", "Laura", "Jamie", "Erica", "Scott",
            "Samuel", "Amy", , "Laura", "Jeffrey", "Crystal", "Jamie", "Mark", "Erica", "Scott", "Jose", "Samuel",
            "Boris"
        ];
        var cf = 0;

        function winners() {
            var name = $("#w_name");

            var moneyHolder = $("#money");
            var face = $("#member_img");
            var money = parseInt($(moneyHolder).html());
            $(moneyHolder).html(rand(45, 400) + '.' + rand(50, 99));
            var n = rand(1, names.length - 1);
            var lf = cf;
            if (jQuery.inArray(n, [3, 5, 6, 7, 8])) {
                while (cf == lf)
                    lf = [3, 5, 6, 7, 8][rand(0, 5)];
                cf = lf;
            }
            if (jQuery.inArray(n, [3, 5, 6, 7, 8])) {
                while (cf == lf)
                    lf = [1, 2, 4, 9, 10][rand(0, 5)];
                cf = lf;
            }

            $(name).html(names[n] + ' ' + "ABCDEFGHIJKLMNOPQRSTUVWXYZ".substr(rand(0, 25), 1) + '.');
            face.attr('src', 'images/people' + n + '.png');
            setTimeout('winners()', rand(7000, 10000));
        }

        yesyoucan = 1;

        $(document).mousemove(function (e) {
            var X = e.pageX;
            var Y = e.pageY;

            if (Y - $(window).scrollTop() > 200)
                mousebottom = 1;

            if (Y - $(window).scrollTop() < 15 && mousebottom == 1 && pu == 1) {
                $('#popup').css('display', 'block');
                mousebottom = 0;
                pu = 0;
            }
        });

        onsubmitfix = 1;

        $(function () {
            copies();
            winners();
        });
    </script>


    <?=$partner?>
</body>

</html>